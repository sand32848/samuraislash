using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakerController : Singleton<CameraShakerController>
{
    [SerializeField] private float maxShake;
    private CinemachineBasicMultiChannelPerlin Perlin;
    private float shakeTime;

    private void Start()
    {
        Perlin = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private void Update()
    {
        shakeTime -= Time.deltaTime;

        if (shakeTime <= 0)
        {
            Perlin.m_AmplitudeGain -= Time.deltaTime * 10;

            Perlin.m_AmplitudeGain = Mathf.Clamp(Perlin.m_AmplitudeGain, 0, maxShake);
        }
    }

    public void ShakeCamera(float _shakeAmount, float _shakeTime)
    {
        Perlin.m_AmplitudeGain += _shakeAmount;

        Perlin.m_AmplitudeGain = Mathf.Clamp(Perlin.m_AmplitudeGain, 0, maxShake);

        if (_shakeTime > shakeTime)
        {
            shakeTime = _shakeTime;
        }
    }
}
