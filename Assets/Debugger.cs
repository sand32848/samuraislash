using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Debugger : MonoBehaviour
{
    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.ReloadScene.started += ReloadScene;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.ReloadScene.started -= ReloadScene;

    }

    public void ReloadScene(InputAction.CallbackContext context)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
