using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private List<Image> heartList = new List<Image>();

    public void UpdateHealthUI(int curHealth,int maxHealth)
    {
        for (int i = curHealth; i < heartList.Count;i++)
        {
            if (!heartList[curHealth]) return;
            heartList[curHealth].color = Color.black;
        }
    }
}
