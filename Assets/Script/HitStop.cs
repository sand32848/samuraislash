using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStop : MonoBehaviour
{
    bool waiting = false;
    public static float duration { get; private set; } = 0.2f;
    private ParryModule parryModule => GetComponent<ParryModule>();

    private void OnEnable()
    {
        parryModule.OnGunParry += Stop;
        parryModule.OnSwordParry += Stop;

    }

    private void OnDisable()
    {
        parryModule.OnGunParry -= Stop;
        parryModule.OnSwordParry -= Stop;
    }

    public void Stop()
    {
        if (waiting)
            return;
        Time.timeScale = 0;
        StartCoroutine(Wait(duration));
    }

    IEnumerator Wait(float duration)
    {
        waiting = true;
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = 1.0f;
        waiting = false;
    }
}
