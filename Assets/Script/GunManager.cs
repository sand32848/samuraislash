using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XInput;

public class GunManager : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private int maxBullet;
    [SerializeField] private int bulletAmount;
    [SerializeField] private float bulletCharge;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private ParticleSystem gunParticle;
    [SerializeField] private Transform ShootPoint;
    [SerializeField] GameObject arm;

    private float _bulletCharge;

    private Vector2 shootDir;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Slash.started += Shoot;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Slash.started -= Shoot;
    }

    private void Update()
    {
        _bulletCharge += Time.deltaTime;

        if(_bulletCharge >= bulletCharge)
        {
            bulletAmount += 1;
            _bulletCharge = 0;
        }

        Vector2 mousePos = Camera.main.ScreenToWorldPoint(InputController.Instance.PlayerAction.Player.MousePos.ReadValue<Vector2>());
        shootDir = (mousePos - (Vector2)ShootPoint.position).normalized;

        var angle = Mathf.Atan2(shootDir.y, shootDir.x) * Mathf.Rad2Deg;
        arm.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    public void Shoot(InputAction.CallbackContext  context)
    {
        if (bulletAmount <= 0) return;
        bulletAmount -= 1;

        var velocity = gunParticle.velocityOverLifetime;
        velocity.x = shootDir.x ;
        velocity.y = shootDir.y ;

        gunParticle.Play();

        RaycastHit2D ray = Physics2D.Raycast(ShootPoint.position, shootDir,50000);

        if (ray)
        {
            print(ray.transform.name);
            if (ray.transform.TryGetComponent(out EnemyHealth enemyHealth))
            {
                enemyHealth.ReduceHealth(damage);
            }

            if (ray.transform.TryGetComponent(out GunReciever gunReciever))
            {
                gunReciever.GetShot(shootDir);
            }
        }
    }
}
