using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    [SerializeField] private GameObject superParticle;
    [SerializeField] private GameObject slashParticle;
    [SerializeField] private GameObject spawnPoint;
    private SlashReciever enemy => GetComponent<SlashReciever>();

    private void OnEnable()
    {
        enemy.OnSlash += SpawnSlashParticle;
    }

    private void OnDisable()
    {
        enemy.OnSlash -= SpawnSlashParticle;
    }

    public void SpawnSlashParticle(Vector2 slashDir)
    {
        Instantiate(slashParticle, spawnPoint.transform.position, Quaternion.identity) ;
    }

    public void SpawnSuperParticle()
    {
        Instantiate(superParticle, spawnPoint.transform.position, superParticle.transform.rotation);
    }
}
