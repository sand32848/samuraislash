
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour
{
    [SerializeField] private List<Image> weaponImage= new List<Image>();
    private void OnEnable()
    {
        ToggleWeapon.OnWeaponSwitch += UpdateWeaponUI;
    }

    private void OnDisable()
    {
        ToggleWeapon.OnWeaponSwitch -= UpdateWeaponUI;
    }

    public void UpdateWeaponUI(int weaponIndex)
    {
        for(int i =0; i < weaponImage.Count; i++)
        {
            weaponImage[i].color = new Color32(100, 100, 100, 255);
        }

        weaponImage[weaponIndex].color = new Color32(255, 255, 255, 255);
    }
}
