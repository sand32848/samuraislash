using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashDestroyModule : MonoBehaviour
{
    SlashReciever slashReciever => GetComponent<SlashReciever>();

    private void OnEnable()
    {
        slashReciever.OnSlash += SlashDestroy;
    }

    private void OnDisable()
    {
        slashReciever.OnSlash -= SlashDestroy;

    }

    public void SlashDestroy(Vector2 _)
    {
        Destroy(gameObject);
    }

}
