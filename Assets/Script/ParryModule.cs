using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ParryModule : MonoBehaviour
{
    [SerializeField] private bool allowGunParry;
    [SerializeField] private bool allowSlashParry;
    [SerializeField] private GameObject parryParticle;
    [SerializeField] private PostProcessProfile parryPostProcess;
    private GunReciever gunReciever => GetComponent<GunReciever>();
    private SlashReciever slashReciever => GetComponent<SlashReciever>();
    public Action OnGunParry;
    public Action OnSwordParry;

    private void OnEnable()
    {
        if (gunReciever) gunReciever.OnShot += GunParry;
        if (slashReciever) slashReciever.OnSlash += SlashParry;
    }

    private void OnDisable()
    {
        if (gunReciever) gunReciever.OnShot += GunParry;
        if (slashReciever) slashReciever.OnSlash += SlashParry;
    }

    public void GunParry(Vector2 gunDir)
    {
        if (!allowGunParry) return;
        OnGunParry?.Invoke();
        InvokeParryEffect();
    }

    public  void SlashParry(Vector2 slashDir)
    {
        if (!allowSlashParry) return;
        OnSwordParry?.Invoke();
        InvokeParryEffect();
    }

    public void InvokeParryEffect()
    {
        PostProcessController.Instance.ChangePostProcess(parryPostProcess,0.1f);
        Destroy(Instantiate(parryParticle, transform.position, Quaternion.identity), 0.1f);
         
    }
}
