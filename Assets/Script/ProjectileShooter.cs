using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    [SerializeField] private float cooldown;
    [SerializeField] private float chargeTime;
    [SerializeField] private Projectile projectile;

    private void Update()
    {
        
    }

    public void InstanceProjectile()
    {
        Instantiate(projectile);
    }
}
