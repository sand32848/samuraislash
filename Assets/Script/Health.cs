using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int health;
    public Action<int,int> OnHealthLose;
    public Action OnDeath;

    public void ReduceHealth(int damage)
    {
        health -= damage;

        OnHealthLose?.Invoke(health,maxHealth);

        if(health <= 0)
        {
            OnDeath?.Invoke();
        }
    }
}
