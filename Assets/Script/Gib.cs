using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Gib : MonoBehaviour
{
    [field:SerializeField] public Rigidbody2D core { get; private set; }
    [SerializeField] private float backwardForce;
    [SerializeField] private float deviation;

    private List<Sprite> spriteList = new List<Sprite>();

    private void Start()
    {
        foreach(SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
        {
            spriteList.Add(sprite.sprite);
        }

        Destroy(gameObject, 5f);
    }

    private void Update()
    {
       if(core) core.AddForce(new Vector2(-backwardForce, 0), ForceMode2D.Force);
    }

    public void OnSpawn()
    {

    }

}
