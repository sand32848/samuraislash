using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody2D rb;
    private SlashReciever slashReciever => GetComponent<SlashReciever>();
    private GunReciever gunReciever => GetComponent<GunReciever>();
    [SerializeField] private int projectileDamage;
    [SerializeField] private float projectileParrySpeed;
    [SerializeField] private float explosionRadius;
    [SerializeField] private GameObject projectileExplosion;
    private bool Parried;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    public void OnSpawn()
    {


    }

    private void OnEnable()
    {
        slashReciever.OnSlash += OnParry;
        gunReciever.OnShot += OnShot;
        
    }

    private void OnDisable()
    {
        slashReciever.OnSlash -= OnParry;
        gunReciever.OnShot -= OnShot;
    }

    public void OnParry(Vector2 slashDir)
    {
        rb.AddForce(slashDir * projectileParrySpeed, ForceMode2D.Impulse);
        Parried = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Parried)
        {
            if(collision.tag != "Player")
            {
                CameraShakerController.Instance.ShakeCamera(4,0.2f);
                AreaDamage();
                Instantiate(projectileExplosion,transform.position,Quaternion.identity);
                Destroy(gameObject);
            }
        }
        else
        {
            if(collision.tag == "Player")
            {
                collision.transform.GetComponent<Health>().ReduceHealth(projectileDamage);
                Destroy(gameObject);
            }
        }
    }

    public void OnShot(Vector2 shotDir)
    {
        AreaDamage();
        Instantiate(projectileExplosion, transform.position, Quaternion.identity);
        CameraShakerController.Instance.ShakeCamera(4, 0.2f);
        Destroy(gameObject, 0.1f);

    }

    public void AreaDamage()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, LayerMask.GetMask("Enemy"));

        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent(out EnemyHealth health))
            {
                health.ReduceHealth(2);
                health.transform.GetComponent<ParticleSpawner>().SpawnSuperParticle();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
