using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashReciever : MonoBehaviour
{
    public Action<Vector2> OnSlash;
    public void GetSlash(Vector2 slashDir)
    {
        OnSlash?.Invoke(slashDir);
    }
}
