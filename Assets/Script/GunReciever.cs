using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunReciever : MonoBehaviour
{
    public Action<Vector2> OnShot;
    public void GetShot(Vector2 shootDir)
    {
        OnShot?.Invoke(shootDir);
    }
}
