using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    public static Action<int, int> OnPlayerHealthChange;

    public static Action OnPlayerDie;

    private void OnEnable()
    {
        OnDeath += PlayerDeath;
        OnHealthLose += OnPlayerHealthChange;
    }

    private void OnDisable()
    {
        OnDeath -= PlayerDeath;
        OnHealthLose -= OnPlayerHealthChange;
    }

    public void PlayerDeath()
    {
        OnPlayerDie?.Invoke();
    }

    public void PlayerHealthChange(int curHealth,int maxHealth)
    {
        OnPlayerHealthChange?.Invoke(curHealth, maxHealth);
    }
}
