using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GibSpawner : MonoBehaviour
{
    [SerializeField] private List<Gib> gibList = new List<Gib>();
    [SerializeField] private GibDirection gibDirection;
    [SerializeField] private float Force;
    [SerializeField] private GameObject spawnPoint;
 
    [SerializeField] private float startDegree;
    [SerializeField] private float rotateRange;
    private Vector2 rotateVector;
    private Vector2 limitA;
    private Vector2 limitB;

    private SlashReciever enemy => GetComponent<SlashReciever>();

    private void OnEnable()
    {
        enemy.OnSlash += SpawnGib;
    }

    private void OnDisable()
    {
        enemy.OnSlash -= SpawnGib;
    }

    public void SpawnGib(Vector2 slashDir)
    {
        limitA = Quaternion.AngleAxis(-rotateRange / 2, Vector3.forward) * StartVector().normalized;
        limitB = Quaternion.AngleAxis(rotateRange / 2, Vector3.forward) * StartVector().normalized;

        for (int i = 0; i < gibList.Count; i++)
        {
            Gib gib = Instantiate(gibList[i], spawnPoint.transform.position, Quaternion.identity);

            var RandomRotationAngle = Random.Range(-rotateRange / 2, rotateRange / 2);
            rotateVector = Quaternion.AngleAxis(RandomRotationAngle, Vector3.forward) * StartVector().normalized;

            gib.core.AddForce(rotateVector * 50, ForceMode2D.Impulse);

        }
    }

    private void OnValidate()
    {
        limitA = Quaternion.AngleAxis(-rotateRange / 2, Vector3.forward) * StartVector().normalized;
        limitB = Quaternion.AngleAxis(rotateRange / 2, Vector3.forward) * StartVector().normalized;
    }

    public Vector2 StartVector()
    {
        return HelperScript.DegreeToVector2(startDegree);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, StartVector().normalized * 5);
        Gizmos.color = Color.white;
        Gizmos.DrawRay(transform.position, rotateVector * 5);
        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, limitA * 5);
        Gizmos.DrawRay(transform.position, limitB * 5);
    }
}


public enum GibDirection {Set,Slash };
