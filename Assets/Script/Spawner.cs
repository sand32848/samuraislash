using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private float spawnTime;
    private float _spawnTime;

    private void Start()
    {
        _spawnTime = spawnTime;    
    }

    void Update()
    {
        _spawnTime -= Time.deltaTime;

        if(_spawnTime <= 0)
        {
            Instantiate(enemy);

            _spawnTime = spawnTime;
        }
    }
}
