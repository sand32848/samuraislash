using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private Vector2 moveDir;
    private Rigidbody2D rb;
    [SerializeField]private float speedX;
    [SerializeField] private float jumpForce;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();    
    }

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed += OnMove;
        InputController.Instance.PlayerAction.Player.Movement.canceled += OnMove;
        InputController.Instance.PlayerAction.Player.Jump.started += OnJump;

    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed -= OnMove;
        InputController.Instance.PlayerAction.Player.Movement.canceled -= OnMove;
        InputController.Instance.PlayerAction.Player.Jump.started -= OnJump;

    }

    private void Update()
    {
        Vector2 movement = new Vector2(moveDir.x * speedX, rb.velocity.y);

        rb.velocity = (movement);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        moveDir = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 2, LayerMask.GetMask("Ground"));

        if (!hit) return;

        rb.AddForce(new Vector2(0, jumpForce),ForceMode2D.Impulse);
    }
}
