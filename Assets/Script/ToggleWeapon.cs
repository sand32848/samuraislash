using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ToggleWeapon : MonoBehaviour
{
    public static Action<int> OnWeaponSwitch;
    [SerializeField] private List<GameObject> weapon = new List<GameObject>();
    [SerializeField] private ParticleSystem weaponSwitchParticle;
    private GameObject currentWeapon;


    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.WeaponSwitch.started += SwitchWeapon;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.WeaponSwitch.started -= SwitchWeapon;
    }

    public void SwitchWeapon(InputAction.CallbackContext context)
    {
        int weaponIndex = (int)context.ReadValue<float>();
        if (weapon[weaponIndex] == currentWeapon) return;

        currentWeapon = weapon[weaponIndex];

        weaponSwitchParticle.Play();

        OnWeaponSwitch?.Invoke((int)context.ReadValue<float>());

        weapon.ForEach(x => x.SetActive(false));

        weapon[(int)context.ReadValue<float>()].SetActive(true);
    }
}
