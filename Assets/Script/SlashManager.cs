using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SlashManager : MonoBehaviour
{
    [SerializeField] private float slashRange;
    [SerializeField] private Transform lineStartPoint;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private ParticleSystem slashParticle;
    [SerializeField] private LayerMask hitMask;

    private Vector2 slashDir;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Slash.started += PerformSlash;
        lineRenderer.enabled = true;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Slash.started -= PerformSlash;
        lineRenderer.enabled = false;

    }

    private void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(InputController.Instance.PlayerAction.Player.MousePos.ReadValue<Vector2>());
        slashDir = (mousePos - (Vector2)lineStartPoint.position).normalized;

        lineRenderer.SetPosition(0, lineStartPoint.position);
        lineRenderer.SetPosition(1, (Vector2)lineStartPoint.position + slashDir * slashRange); ;

        var angle = Mathf.Atan2(slashDir.y, slashDir.x) * Mathf.Rad2Deg;
        lineStartPoint.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void PerformSlash(InputAction.CallbackContext context)
    {
        slashParticle.Play();

        RaycastHit2D ray = Physics2D.Raycast(lineStartPoint.position, slashDir, slashRange,hitMask);

        if (ray)
        {
            print(ray);

           if(ray.transform.TryGetComponent(out SlashReciever slashReceiver))
            {
                slashReceiver.GetSlash(slashDir);
            }
        }
    }
}
