using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health
{
    [SerializeField] private bool destroyOnDeath = true;

    private void OnEnable()
    {
        OnDeath += DestroyOnDeath;
    }

    private void OnDisable()
    {
        OnDeath -= DestroyOnDeath;

    }

    public void DestroyOnDeath()
    {
        if (destroyOnDeath)
        {
            Destroy(gameObject);
        }
    }

}

