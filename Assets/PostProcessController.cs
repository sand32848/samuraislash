using Cinemachine.PostFX;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : Singleton<PostProcessController>
{
    [SerializeField] private CinemachinePostProcessing cinemachinePostProcessing;

    public void ChangePostProcess(PostProcessProfile postProcessProfile = null)
    {
        if (postProcessProfile)
        {
            cinemachinePostProcessing.m_Profile = postProcessProfile;
        }
        else
        {
            cinemachinePostProcessing.m_Profile = null;
        }
    }

    public void ChangePostProcess(PostProcessProfile postProcessProfile, float time)
    {
        StartCoroutine(PostProcessWait(postProcessProfile,time));
    }

    IEnumerator PostProcessWait(PostProcessProfile postProcessProfile, float time)
    {
        cinemachinePostProcessing.m_Profile = postProcessProfile;
        yield return new WaitForSeconds(time);
        cinemachinePostProcessing.m_Profile = null;
    }


}
